package bettingapp.bettingapp;

import android.support.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


public class LoginTests {
    private String userString;
    private String passwordString;

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<>(
            LoginActivity.class);

    @Before
    public void initValidStrings() {
        // Specify a valid string.
        userString = "Pasimies";
        passwordString = "salasana";
    }

    @Test
    public void typeLoginTest() {
        // Type text
        onView(withId(R.id.username))
                .perform(typeText(userString), closeSoftKeyboard());

        onView(withId(R.id.password))
                .perform(typeText(passwordString), closeSoftKeyboard());


        // Check that the texts were changed.
        onView(withId(R.id.username))
                .check(matches(withText(userString)));

        onView(withId(R.id.password))
                .check(matches(withText(passwordString)));


    }

    @Test
    public void clickLoginTest() {
        onView(withId(R.id.loginbutton)).perform(click())
                .check(matches(withId(R.id.profile_layout)));
    }
}
