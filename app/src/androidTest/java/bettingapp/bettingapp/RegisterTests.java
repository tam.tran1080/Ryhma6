package bettingapp.bettingapp;

import android.support.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


public class RegisterTests {

    private String userString;
    private String passwordString;
    private String emailString;

    @Rule
    public ActivityTestRule<RegisterActivity> mActivityRule = new ActivityTestRule<>(
            RegisterActivity.class);

    @Before
    public void initValidStrings() {
        // Specify a valid string.
        userString = "Pasimies";
        emailString = "pasi.mies@gmail.com";
        passwordString = "salasana";
    }
    @Test
    public void typeRegisterTest() {
        onView(withId(R.id.regemail))
                .perform(typeText(emailString), closeSoftKeyboard());

        onView(withId(R.id.reguser))
                .perform(typeText(userString), closeSoftKeyboard());

        onView(withId(R.id.regpassword))
                .perform(typeText(passwordString), closeSoftKeyboard());

        onView(withId(R.id.regemail))
                .check(matches(withText(emailString)));

        onView(withId(R.id.username))
                .check(matches(withText(userString)));

        onView(withId(R.id.password))
                .check(matches(withText(passwordString)));
    }

    @Test
    public void clickRegisterTest() {
        onView(withId(R.id.regbutton)).perform(click())
                .check(matches(withId(R.id.activity_login)));
    }
}
