package bettingapp.bettingapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import bettingapp.bettingapp.databaseFunctions.Constants;

/**
 * Created by Kairamo on 20.4.2017.
 */

public class BetMenu extends AppCompatActivity implements View.OnClickListener {

    private String kohde;
    private String valittuKerroin;
    private String kerroin;
    private String laji;
    final String userID = SharedPreferenceManager.getInstance(this).getUserID();
    Button button1;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pop);

        progressDialog = new ProgressDialog(this);
        button1 = new Button(this);


        Bundle bundle = getIntent().getExtras();

        //halutun vedonlyöntikohteen tiedot. kohde, veto(1x2) ja kerroin
        kohde = bundle.getString("kohde"); //Nämä tiedot databasseen kun ok nappia painettu
        valittuKerroin = bundle.getString("veto");
        kerroin = bundle.getString("kerroin");
        laji = bundle.getString("laji");


        //Popup-menun konffausta
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width * .8), (int)(height *.65));


        //Vetokohteen tulostaminen popup-menuun, sekä ok buttonin luonti
        TextView textKohde = new TextView(this);
        textKohde.setText(kohde);
        TextView textVeto = new TextView(this);
        textVeto.setText(valittuKerroin);
        TextView textKerroin = new TextView(this);
        textKerroin.setText(kerroin);
        textKerroin.setGravity(Gravity.CENTER_HORIZONTAL);
        textKohde.setGravity(Gravity.CENTER_HORIZONTAL);
        textVeto.setGravity(Gravity.CENTER_HORIZONTAL);
        textKohde.setTextSize(30);
        textVeto.setTextSize(30);
        textKerroin.setTextSize(30);

        button1.setOnClickListener(this);
        button1.setText("ok");

        View linearLayout = findViewById(R.id.pop_activity);

        LinearLayout ll = (LinearLayout) linearLayout;

        ll.addView(textKohde);
        ll.addView(textVeto);
        ll.addView(textKerroin);
        ll.addView(button1);

    }

    //Sends placed bet to the database
    private void saveBet(final String panos){
        //Progress dialog to show information to user about how request is going
        progressDialog.setMessage("Placing bet...");
        progressDialog.show();

        //Builds string request that will be sent to the database
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_SAVEBETS,
                new Response.Listener<String>(){
                    @Override
                    //Defines what happens when response from server arrives
                    public void onResponse(String response){
                        progressDialog.dismiss();
                        try {
                            //Creates JSONObject based on response String (comes from the server)
                            JSONObject jsonObject = new JSONObject(response);
                            //Prints message from server that is saved in jsonObject
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            if(!jsonObject.getBoolean("error")){
                                //Decreases users money that is visible to user
                                //SharedPreferenceManager.getInstance(getApplicationContext()).decreaseUserMoney(panos);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    //Prints error if any
                    public void onErrorResponse(VolleyError error){
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            //Contains parameters to be sent to the database
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userID);
                params.put("game_name", kohde);
                params.put("selected_multiplier", valittuKerroin);
                params.put("multiplier", kerroin);
                params.put("bet_amount", panos);
                params.put("sport", laji);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
        finish(); // Shuts down the popupmenu after placing a bet
    }



    // ok buttonia painettaessa tiedot lähetettävä databaseen
    // vetokohde, laji, veikkaus(1, x vai 2), kerroin sekä panos
    // vedon lähetettyä pelaajan varoja on vähennettävä panoksen verran
    // vedon tiedot on tallennettuna muuttujiin jotka löytyvät tämän activityn yläosasta
    @Override
    public void onClick(View view) {

        //panoksen poimiminen panos-kentästä
        if(view == button1){
            EditText panosKenttä = (EditText)findViewById(R.id.editText2);

            //Changing string values to double values
            double fake_money = Double.parseDouble(SharedPreferenceManager.getInstance(this).getKeyUserMoney());
            String panos = panosKenttä.getText().toString();

            //Compares users current money to set bet value to make sure user can afford the bet
            if(fake_money > Double.parseDouble(panos)){

                saveBet(panos);
                Log.d("panos" , panos);
            }else{
                Toast.makeText(this, "You can't afford that bet", Toast.LENGTH_LONG).show();

            }




        }



    }
}
