package bettingapp.bettingapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Kairamo on 6.3.2017.
 */

public class FootballActivity extends BaseActivity implements OnClickListener {
    static int apu;

   /* @Override
    public int getLayoutResource() {
        return R.layout.activity_main;
    }*/


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_football);


        Games gam = new Games();
        gam.execute();


        View linearLayout = findViewById(R.id.container);

        LinearLayout ll = (LinearLayout) linearLayout;

        for( int i = 0; i < gam.getGames().size(); i++ )
        {
            TextView aika = new TextView(this);
            aika.setText(gam.getGames().get(i).getAika());

            aika.setGravity(Gravity.CENTER_HORIZONTAL);
            aika.setTextColor(Color.GRAY);
            ll.addView(aika);

            TextView textView = new TextView(this);
            textView.setId(1000 + i);
            textView.setText(gam.getGames().get(i).getJoukkueet());
            textView.setTextSize(30);
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
            textView.setTextColor(Color.DKGRAY);
            ll.addView(textView);





            Button button1 = new Button(this);
            Button buttonx = new Button(this);
            Button button2 = new Button(this);
            button1.setId(2000 + i);
            buttonx.setId(3000 + i);
            button2.setId(4000 + i);
            button1.setOnClickListener(this);
            buttonx.setOnClickListener(this);
            button2.setOnClickListener(this);

            button1.setText("1\n" + (gam.getGames().get(i).getKerroin1()));
            buttonx.setText("x\n" + (gam.getGames().get(i).getKerroinx()));
            button2.setText("2\n" + (gam.getGames().get(i).getKerroin2()));


            ll.addView(button1);
            ll.addView(buttonx);
            ll.addView(button2);
            /*
            TextView textView2 = new TextView(this);
            textView2.setText("1  " + (gam.getGames().get(i).getKerroin1()) + "    x  " + (gam.getGames().get(i).getKerroinx()) + "    2  " + (gam.getGames().get(i).getKerroin2()));
            linearLayout.addView(textView2);
            */

        }
   /*     LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        for( int i = 0; i < gam.getGames().size(); i++ )
        {
            TextView textView = new TextView(this);
            textView.setText(gam.getGames().get(i).getJoukkueet());
            linearLayout.addView(textView);
        }*/
/*
        ArrayList<Ottelu> pelit = gam.getOttelut();
        for(Ottelu o : pelit) {
            Log.d("kenkälusikka", o.toString());
        }
        */

    }

    @Override
    public void onClick(View view) {

        Log.d("view.getId()" , Integer.toString(view.getId()));
        Intent intent = new Intent(FootballActivity.this,BetMenu.class);
        if(view.getId() < 3000){
            int text = view.getId() - 1000;
            TextView pel = (TextView)findViewById(text);
            Button b = (Button)view;
            String buttonText = b.getText().toString();
            String lines[] = buttonText.split("\\r?\\n");
            Log.d("view.getId()" , lines[0]); // Tulostaa 1
            Log.d("view.getId()" , lines[1]); // Tulostaa kertoimen
            Log.d("view.getId()" , ((TextView) findViewById(text)).getText().toString()); // Tulostaa kohteen

            intent.putExtra("veto", lines[0]);
            intent.putExtra("kerroin", lines[1]);
            intent.putExtra("kohde", ((TextView) findViewById(text)).getText().toString());
            intent.putExtra("laji", "Jalkapallo");

        } else if (view.getId() < 4000){
            int text = view.getId() - 2000;
            TextView pel = (TextView)findViewById(text);
            Button b = (Button)view;
            String buttonText = b.getText().toString();
            String lines[] = buttonText.split("\\r?\\n");
            Log.d("view.getId()" , lines[0]); // Tulostaa x
            Log.d("view.getId()" , lines[1]); // Tulostaa kertoimen
            Log.d("view.getId()" , ((TextView) findViewById(text)).getText().toString()); // Tulostaa kohteen

            intent.putExtra("veto", lines[0]);
            intent.putExtra("kerroin", lines[1]);
            intent.putExtra("kohde", ((TextView) findViewById(text)).getText().toString());
            intent.putExtra("laji", "Jalkapallo");


        } else {
            int text = view.getId() - 3000;
            TextView pel = (TextView)findViewById(text);
            Button b = (Button)view;
            String buttonText = b.getText().toString();
            String lines[] = buttonText.split("\\r?\\n");
            Log.d("view.getId()" , lines[0]); // Tulostaa 2
            Log.d("view.getId()" , lines[1]); // Tulostaa kertoimen
            Log.d("view.getId()" , ((TextView) findViewById(text)).getText().toString()); // Tulostaa kohteen

            intent.putExtra("veto", lines[0]);
            intent.putExtra("kerroin", lines[1]);
            intent.putExtra("kohde", ((TextView) findViewById(text)).getText().toString());
            intent.putExtra("laji", "Jalkapallo");

        }
        startActivity(intent);

    }

    // Taustalla pyörivä koodi joka hakee ottelut ja kertoimet
    private class Games extends AsyncTask<Void, Void, Void> {

        private ArrayList<String> games = new ArrayList<String>();
        private ArrayList<String> kertoimet = new ArrayList<String>();
        private ArrayList<String> aika = new ArrayList<String>();
        private ArrayList<Ottelu> ottelut = new ArrayList<Ottelu>();
        private boolean trigger = true;
        private Document docu = null;




        @Override
        protected Void doInBackground(Void... params) {
            // yhteys sivustoon
            try {
                docu = Jsoup.connect("http://annabet.com/en/oddcomp/?w=soccerstats&booker=12").get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Hakee otteluissa pelaavien joukkueiden nimet
            for (Element row : docu.select(".nicelight")) {
                for (Element row1 : row.select("td")) {
                    if (row1.text().contains(" - ") && trigger) {
                        String tul = row1.text();
                        games.add(tul);
                        // vaihtaa trigger muuttujan arvon jotta koodi hakee seuraavaksi kertoimet eikä uutta ottelua
                        trigger = false;
                    }
                    // hakee kertoimet (1, x ja 2)
                    if (apu > 0) {
                        String tulo = row1.text();
                        kertoimet.add(tulo);
                        apu--;
                    }
                    //Etsii eri vedonlyöntitoimistojen nimiä, jos löytyy asettaa apu-muuttujan arvoon 3, jolloin koodi seuraavilla kierroksilla osaa poimia kertoimet.
                    if (row1.text().contains("Veikkaus") && !trigger) {
                        apu = 3;
                        trigger = true;
                    } else if (row1.text().contains("Betsson") && !trigger) {
                        apu = 3;
                        trigger = true;
                    } else if (row1.text().contains("Bet-at-home") && !trigger) {
                        apu = 3;
                        trigger = true;
                    } else if (row1.text().contains("Pinnacle") && !trigger) {
                        apu = 3;
                        trigger = true;
                    } else if (row1.text().contains("Betfair") && !trigger) {
                        apu = 3;
                        trigger = true;
                    } else if (row1.text().contains("Bet365") && !trigger) {
                        apu = 3;
                        trigger = true;
                    } else if (row1.text().contains("BetRedKings") && !trigger) {
                        apu = 3;
                        trigger = true;
                    } else if (row1.text().contains("Unibet") && !trigger) {
                        apu = 3;
                        trigger = true;
                    } else if (row1.text().contains("ComeOn") && !trigger) {
                        apu = 3;
                        trigger = true;
                    }


                }

            }
            // Hakee otteluiden ajankohdat
            for (Element row : docu.select(".verysmall")) {

                aika.add(row.text());
            }
//        for(Element row : docu.select(".nicelight")){
//            for(Element row3 : row.select("td")){
//            	System.out.println(row3.text());
//
//            }
//        }
            //Luo Otteluista oliot ja lisää ne listaan
            int x = 0;
            int y = 0;
            for (String g : games) {
                Ottelu ott = new Ottelu();
                ott.joukkueet = g;
                ott.kerroin1 = kertoimet.get(x);
                x++;
                ott.kerroinx = kertoimet.get(x);
                x++;
                ott.kerroin2 = kertoimet.get(x);
                x++;
                ott.aika = aika.get(y);
                y++;
                ottelut.add(ott);
            }




            for(Ottelu o : ottelut) {
                Log.d("kenkälusikka", o.toString());



            }

            return null;
        }

        protected ArrayList<Ottelu> getGames(){

            while (ottelut.size() < 5){

            }


            return ottelut;

        }

    }
}



