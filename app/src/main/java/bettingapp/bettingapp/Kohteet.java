package bettingapp.bettingapp;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Created by Kairamo on 6.2.2017.
 */

public class Kohteet  {



    static int apu;
    private ArrayList<String> games = new ArrayList<String>();
    private ArrayList<String> kertoimet = new ArrayList<String>();
    private ArrayList<String> aika = new ArrayList<String>();
    private ArrayList<Ottelu> ottelut = new ArrayList<Ottelu>();
    private boolean trigger = true;
    private Document docu = null;

    private Kohteet() {

        try {
            docu = Jsoup.connect("http://annabet.com/fi/oddcomp/?w=soccerstats&booker=5").get();
        } catch (IOException a) {
            System.out.println("error");
        }


        for (Element row : docu.select(".nicelight")) {
            for (Element row1 : row.select("td")) {
                if (row1.text().contains(" - ") && trigger) {
                    String tul = row1.text();


                    games.add(tul);
                    trigger = false;


                }
                if (apu > 0) {
                    String tulo = row1.text();

                    kertoimet.add(tulo);
                    apu--;
                }
                if (row1.text().contains("Veikkaus") && !trigger) {
                    apu = 3;
                    trigger = true;
                } else if (row1.text().contains("Betsson") && !trigger) {
                    apu = 3;
                    trigger = true;
                } else if (row1.text().contains("Bet-at-home") && !trigger) {
                    apu = 3;
                    trigger = true;
                } else if (row1.text().contains("Pinnacle") && !trigger) {
                    apu = 3;
                    trigger = true;
                } else if (row1.text().contains("Betfair") && !trigger) {
                    apu = 3;
                    trigger = true;
                } else if (row1.text().contains("Bet365") && !trigger) {
                    apu = 3;
                    trigger = true;
                } else if (row1.text().contains("BetRedKings") && !trigger) {
                    apu = 3;
                    trigger = true;
                } else if (row1.text().contains("Unibet") && !trigger) {
                    apu = 3;
                    trigger = true;
                } else if (row1.text().contains("ComeOn") && !trigger) {
                    apu = 3;
                    trigger = true;
                }


            }

        }
        for (Element row : docu.select(".verysmall")) {

            aika.add(row.text());
        }
//        for(Element row : docu.select(".nicelight")){
//            for(Element row3 : row.select("td")){
//            	System.out.println(row3.text());
//
//            }
//        }
        int x = 0;
        int y = 0;
        for (String g : games) {
            Ottelu ott = new Ottelu();
            ott.joukkueet = g;
            ott.kerroin1 = kertoimet.get(x);
            x++;
            ott.kerroinx = kertoimet.get(x);
            x++;
            ott.kerroin2 = kertoimet.get(x);
            x++;
            ott.aika = aika.get(y);
            y++;
            ottelut.add(ott);
        }


    }

    public ArrayList<Ottelu> getOttelut() {
        return ottelut;
    }


    public void setGames(ArrayList<String> games) {
        this.games = games;
    }
}


