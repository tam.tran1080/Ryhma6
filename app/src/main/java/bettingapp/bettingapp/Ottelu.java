package bettingapp.bettingapp;

/**
 * Created by Kairamo on 13.2.2017.
 */

public class Ottelu {
    String joukkueet;
    String aika;
    String kerroin1;
    String kerroinx;
    String kerroin2;

    public Ottelu(){

    }

    public String getJoukkueet() {
        return joukkueet;
    }

    public void setJoukkueet(String joukkueet) {
        this.joukkueet = joukkueet;
    }

    public String getAika() {
        return aika;
    }

    public String getKerroinx() {
        return kerroinx;
    }

    public String getKerroin1() {
        return kerroin1;
    }

    public String getKerroin2() {
        return kerroin2;
    }

    public void setAika(String aika) {
        this.aika = aika;
    }


    public void setKerroin1(String kerroin1) {
        this.kerroin1 = kerroin1;
    }

    public void setKerroinx(String kerroinx) {
        this.kerroinx = kerroinx;
    }

    public void setKerroin2(String kerroin2) {
        this.kerroin2 = kerroin2;
    }

    @Override
    public String toString() {
        return "Ottelu [joukkueet=" + joukkueet + ", aika=" + aika + ", kerroin1=" + kerroin1 + ", kerroinx=" + kerroinx
                + ", kerroin2=" + kerroin2 + "]";
    }
}
