package bettingapp.bettingapp;

/**
 * Created by Tam on 9.3.2017.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import bettingapp.bettingapp.databaseFunctions.Constants;

public class ProfileActivity extends BaseActivity {

    Button infobtn, actbetbtn, winbtn, losebtn, miscbtn;
    TextView infoview, actbetview, winview, loseview, miscview;
    String info, actbet, win, lose, misc;
    ProgressDialog progressDialog; //Progressbar to show information
    private TextView textViewUsername, textViewUserEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actibity_profile_design_ui); // all these activities are shown in profile ui design

        // user information

                if(!SharedPreferenceManager.getInstance(this).isLoggedIn()){

                    finish();
                    startActivity(new Intent(this, LoginActivity.class));
                }
                // shows log in page

                textViewUsername = (TextView) findViewById(R.id.textViewUsername);
                textViewUserEmail = (TextView) findViewById(R.id.textViewUseremail);
                // shows data in mentioned views

                textViewUsername.setText(SharedPreferenceManager.getInstance(this).getUsername());
                textViewUserEmail.setText(SharedPreferenceManager.getInstance(this).getUserEmail());
                // getting the data for the views


        // shows user information
        infobtn = (Button) findViewById(R.id.infobtn);
        infobtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ProfileActivity.this, infobtn);
                //Inflating the Popup using xml file
                /*popup.getMenuInflater()
                        .inflate(R.menu.activity_user_data, popup.getMenu());*/

                //registering info with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(
                                ProfileActivity.this,
                                "You Clicked : " + item.getTitle(),
                                Toast.LENGTH_SHORT
                        ).show();
                        return true;

                    }
                });
                popup.show(); //showing info menu

            }
        }); //closing the setOnClickListener method

        // shows active bets
        actbetbtn = (Button) findViewById(R.id.actbetbtn);
        actbetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ProfileActivity.this, actbetbtn);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.actbetbtn, popup.getMenu());

                //registering info with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(
                                ProfileActivity.this,
                                "You Clicked : " + item.getTitle(),
                                Toast.LENGTH_SHORT
                        ).show();
                        return true;

                    }
                });

                popup.show(); //showing info menu
            }
        }); //closing the setOnClickListener method


        // shows won bets
        winbtn = (Button) findViewById(R.id.winbtn);
        winbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ProfileActivity.this, winbtn);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.winbtn, popup.getMenu());

                //registering info with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(
                                ProfileActivity.this,
                                "You Clicked : " + item.getTitle(),
                                Toast.LENGTH_SHORT
                        ).show();
                        return true;
                    }
                });

                popup.show(); //showing info menu
            }
        }); //closing the setOnClickListener method


        // shows lost bets
        losebtn = (Button) findViewById(R.id.losebtn);
        losebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ProfileActivity.this, losebtn);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.losebtn, popup.getMenu());

                //registering info with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(
                                ProfileActivity.this,
                                "You Clicked : " + item.getTitle(),
                                Toast.LENGTH_SHORT
                        ).show();
                        return true;
                    }
                });

                popup.show(); //showing info menu
            }
        }); //closing the setOnClickListener method


        // shows other
        miscbtn = (Button) findViewById(R.id.miscbtn);
        miscbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(ProfileActivity.this, miscbtn);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.miscbtn, popup.getMenu());

                //registering info with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(
                                ProfileActivity.this,
                                "You Clicked : " + item.getTitle(),
                                Toast.LENGTH_SHORT
                        ).show();
                        return true;
                    }
                });

                popup.show(); //showing info menu
            }
        }); //closing the setOnClickListener method




    } // closing of onCreate



    //Gets users placed bets from database
    public void getUserBets(){
        progressDialog.setMessage("Registering user...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_REGISTER,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response){
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            /*Structure of successful jsonObject
                            ['error',
                            'message',
                            'content'{
                                'betID',
                                'userID',
                                'game_name',
                                'selected_multiplier',
                                'multiplier',
                                'bet_amount',
                                'sport'
                            }
                                                            ]
                            */

                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show(); // Prints message from json object
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", SharedPreferenceManager.getInstance(getApplicationContext()).getUserID());

                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }



} // closing of class
