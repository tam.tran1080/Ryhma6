package bettingapp.bettingapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import bettingapp.bettingapp.databaseFunctions.Constants;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText reguser, regpassword, regemail;
    private Button regbutton, cancelbutton;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Getting instances of GUI widgets
        reguser = (EditText) findViewById(R.id.reguser);
        regpassword = (EditText) findViewById(R.id.regpassword);
        regemail = (EditText) findViewById(R.id.regemail);

        regbutton = (Button) findViewById(R.id.regbutton);
        cancelbutton = (Button) findViewById(R.id.cancelbutton);
        progressDialog = new ProgressDialog(this);

        regbutton.setOnClickListener(this);
        cancelbutton.setOnClickListener(this);
    }
    private void registerUser(){
        final String email = regemail.getText().toString().trim();
        final String username = reguser.getText().toString().trim();
        final String password = regpassword.getText().toString().trim();

        progressDialog.setMessage("Registering user...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_REGISTER,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response){
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            if(!jsonObject.getBoolean("error")){
                                finish();
                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void onClick(View view){
        if(view == regbutton){ // If register button is pressed -> register user
            registerUser();
        }if(view == cancelbutton){
            startActivity(new Intent(this, LoginActivity.class));
        }


    }
}
