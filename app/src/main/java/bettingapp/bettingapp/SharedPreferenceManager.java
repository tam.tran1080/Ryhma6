package bettingapp.bettingapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by joni.
 *
 * Using singleton pattern
 */

public class SharedPreferenceManager {
    private static SharedPreferenceManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "mysharedpref";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_USER_EMAIL = "useremail";
    private static final String KEY_USER_ID = "userid";
    private static final String KEY_USER_MONEY = "fake_money";

    private SharedPreferenceManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPreferenceManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreferenceManager(context);
        }
        return mInstance;
    }

    public boolean userLogin(int id, String username, String email, double fake_money){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_USER_ID, id+"");
        editor.putString(KEY_USER_EMAIL, email);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_USER_MONEY, fake_money+""); // Muutetaan double stringiksi jotta sitä voidaan käyttää editorissa
        editor.apply();

        return true;
    }

    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if(sharedPreferences.getString(KEY_USERNAME, null) != null){
            return true;
        }
        return false;
    }

    public void logout(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();

        //Uloskirjautumisen jälkeen käyttäjä ohjataan takaisin kirjautumissivulle
        Intent i = new Intent(mCtx, LoginActivity.class);
        //Suljetaan kaikki activityt
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //Lisätään uusi "lippu" uuden activityn avaamiseksi
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //Käynnistetään LoginActivity
        mCtx.startActivity(i);

    }


    public String getUsername(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME, null);
    }
    public String getUserEmail(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_EMAIL, null);
    }
    public String getUserID(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_ID, null);
    }
    public String getKeyUserMoney(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_MONEY, null);
    }
    public void setUserMoney(String value){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_MONEY, value);
        editor.apply();
    }


  /*  //Increases users money in SharedPreferences
    //Using string as parameter because SP doesn't support more precise value for numerals
    public void increaseUserMoney(String amount){
        double currentMoney = Double.parseDouble(getKeyUserMoney());
        double doubleAmount = Double.parseDouble(amount);
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_MONEY, (currentMoney+doubleAmount)+"");
        editor.apply();
    }
    //Decreases users money in SP
    public void decreaseUserMoney(String amount){
        double currentMoney = Double.parseDouble(getKeyUserMoney());
        double doubleAmount = Double.parseDouble(amount);
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_USER_MONEY, (currentMoney-doubleAmount)+"");
        editor.apply();
    }
*/
}