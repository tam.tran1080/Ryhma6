package bettingapp.bettingapp.databaseFunctions;

/**
 * Created by joni on 14.02.2017.
 *
 * Jos ohjelmaa käytetään koulusta, tulee ROOT_URL:in olla "http://10.114.32.5/".
 * Jos ohjelmaa käytetään muualta Puttyn kautta, tulee tunnelin olla portissa 2280
 * ja ROOT_URL:in täytyy olla "http://10.0.2.2:2280/".
 */



public class Constants {
    private static final String ROOT_URL = "http://10.0.2.2/Bettingapp/v1/";

    public static final String URL_REGISTER = ROOT_URL+"registerUser.php";
    public static final String URL_LOGIN = ROOT_URL+"userLogin.php";
    public static final String URL_SAVEBETS = ROOT_URL+"saveBets.php";
}
